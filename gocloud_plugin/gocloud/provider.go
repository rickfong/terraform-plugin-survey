package gocloud
  
import (
    "github.com/hashicorp/terraform-plugin-sdk/helper/mutexkv"
    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
    "github.com/hashicorp/terraform-plugin-sdk/terraform"
)

var osMutexKV = mutexkv.NewMutexKV()

type PConfig struct {
    Config
}

// Provider returns a schema.Provider for GOC.
func Provider() terraform.ResourceProvider {
    provider := &schema.Provider{
        Schema: map[string]*schema.Schema{
            "username": {
                Type:		schema.TypeString,
                Required:	true,
                DefaultFunc:	schema.EnvDefaultFunc("GOC_USERNAME", ""),
                Description:	descriptions["username"],
            },
            "password": {       
                Type:		schema.TypeString,
                Required:	true,
                DefaultFunc:	schema.EnvDefaultFunc("GOC_PASSWORD", ""),
                Description:	descriptions["password"],
            },
            "goc_url": {       
                Type:		schema.TypeString,
                Required:	true,
                DefaultFunc:	schema.EnvDefaultFunc("GOC_URL", ""),
                Description:	descriptions["goc_url"],
            },
        },

        DataSourcesMap: map[string]*schema.Resource{
            "gocloud_solution":		dataSourceSolution(),
            "gocloud_project":		dataSourceProject(),
        },

        ResourcesMap: map[string]*schema.Resource{
            "gocloud_project":          resourceProject(),
            "gocloud_solution":		resourceSolution(),
            "gocloud_volume":		resourceVolume(),
        },
    }

    provider.ConfigureFunc = func(d *schema.ResourceData) (interface{}, error) {
        terraformVersion := provider.TerraformVersion
        if terraformVersion == "" {
            // Terraform 0.12 introduced this field to the protocol
            // We can therefore assume that if it's missing it's 0.10 or 0.11
            terraformVersion = "0.11+compatible"
        }
        return configureProvider(d, terraformVersion)
    }
    return provider
}

var descriptions map[string]string

func init() {
    descriptions = map[string]string{
        "username": "Username to login with.",
        "password": "Password to login with.",
        "goc_url": "GOC endpoint to request to.",
    }
}

func configureProvider(d *schema.ResourceData, terraformVersion string) (interface{}, error) {
    config := PConfig{
        Config{
            GOC_USERNAME:	d.Get("username").(string),
            GOC_PASSWORD:	d.Get("password").(string),
            GOC_URL:		d.Get("goc_url").(string),
        },
    }

    if err := config.LoadAndValidate(); err != nil {
        return nil, err
    }

    return &config, nil
}
