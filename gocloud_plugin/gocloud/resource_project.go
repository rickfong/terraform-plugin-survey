package gocloud

import (
    "bytes"
    "encoding/json"
    "fmt"
    "log"
    "time"

    "github.com/hashicorp/terraform-plugin-sdk/helper/resource"
    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

type ProjectBody struct {
    Name		string	`json:"name"`
    Desc		string	`json:"desc,omitempty"`
}

func resourceProject() *schema.Resource {
    return &schema.Resource{
        Create: resourceProjectCreate,
        Read:   resourceProjectRead,
        Delete: resourceProjectDelete,
        Importer: &schema.ResourceImporter{
            State: schema.ImportStatePassthrough,
        },

        Schema: map[string]*schema.Schema{
            "desc": {
                Type:		schema.TypeString,
                Optional:	true,
                ForceNew:	true,
            },

            "name": {
                Type:		schema.TypeString,
                Required:	true,
                ForceNew:       true,
            },

            "platform": {
                Type:		schema.TypeString,
                Required:	true,
                ForceNew:       true,
            },

            "project_id": {     
                Type:		schema.TypeInt,
                Optional:	true,
                Computed:	true,
                ForceNew:	true,
            },

            "status": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "status_reason": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "uuid": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },
        },
    }
}

func resourceProjectCreate(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    platform := d.Get("platform").(string)
    resourcePath := fmt.Sprintf("%s/projects/", platform)
    name := d.Get("name").(string)
    desc := d.Get("desc").(string)
    body := ProjectBody {
        Name:			name,
        Desc:			desc,
    }

    buf := new(bytes.Buffer)
    json.NewEncoder(buf).Encode(body)
    response, err := config.doNormalRequest(resourcePath, "POST", buf)

    if err != nil {
        return fmt.Errorf("Error creating gocloud_project %s on %s: %s", name, platform, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }

    projectID := int(data["id"].(float64))
    stateConf := &resource.StateChangeConf{
        Pending:    []string{"Initializing"},
        Target:     []string{"Ready"},
        Refresh:    projectStateRefreshFunc(config, resourcePath, projectID),
        Timeout:    d.Timeout(schema.TimeoutCreate),
        Delay:      10 * time.Second,
        MinTimeout: 3 * time.Second,
    }

    _, err = stateConf.WaitForState()
    if err != nil {
        return fmt.Errorf(
            "Error waiting for gocloud_project %d to become ready: %s", projectID, err)
    }

    d.SetId(fmt.Sprintf("%v", data["id"]))
    d.Set("project_id", data["id"])
    d.Set("platform", platform)
    return resourceProjectRead(d, meta)
}

func resourceProjectRead(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    projectID := d.Id()
    projectPlatform := d.Get("platform").(string)
    resourcePath := fmt.Sprintf("%s/projects/%s/", projectPlatform, projectID)
    response, err := config.doNormalRequest(resourcePath, "GET", nil)

    if err != nil {
        return fmt.Errorf("Unable to retrieve project %s on %s: %s", projectID, projectPlatform, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return fmt.Errorf("Unable to retrieve project %s on %s: %s", projectID, projectPlatform, err)
    }

    log.Printf("[DEBUG] Retrieved gocloud_project %s", d.Id())

    d.Set("name", data["name"])
    d.Set("status", data["status"])
    d.Set("status_reason", data["status_reason"])
    d.Set("uuid", data["uuid"])
    return nil
}

func resourceProjectDelete(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    projectID := d.Id()
    projectPlatform := d.Get("platform").(string)
    resourcePath := fmt.Sprintf("%s/projects/%s/", projectPlatform, projectID)
    _, err := config.doNormalRequest(resourcePath, "DELETE", nil)

    if err != nil {
        return fmt.Errorf("Unable to delete solution %s: on %s %s", projectID, projectPlatform, err)
    }

    d.SetId("")

    return nil
}
