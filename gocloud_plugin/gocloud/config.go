package gocloud

import (
    "bytes"
    "fmt"
)

type Config struct {
    GOC_USERNAME	string
    GOC_PASSWORD	string
    GOC_URL		string

    GOCClient		*ProviderClient
}

func (c *Config) LoadAndValidate() error {
    if c.GOC_USERNAME == "" {
        return fmt.Errorf("'GOC_USERNAME' must be specified")
    }

    if c.GOC_PASSWORD == "" {
        return fmt.Errorf("'GOC_PASSWORD' must be specified")
    }

    if c.GOC_URL == "" {
        return fmt.Errorf("'GOC_URL' must be specified")
    }

    client := newProviderClient(c.GOC_USERNAME, c.GOC_PASSWORD, c.GOC_URL)
    c.GOCClient = client

    return nil
}

func (c *Config) doNormalRequest (
        resourcePath string,
        method string,
        body *bytes.Buffer) (string, error) {
    response, err := c.GOCClient.doRequest(resourcePath, method, body)
    return response, err
}
