package gocloud

import (
    "bytes"
    "encoding/json"
    "fmt"
    "log"
    "time"

    "github.com/hashicorp/terraform-plugin-sdk/helper/resource"
    "github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

type VolumePostBody struct {
    Name		string	`json:"name"`
    Project		int	`json:"project,omitempty"`
    Size		int	`json:"size,omitempty"`
}

func resourceVolume() *schema.Resource {
    return &schema.Resource{
        Create: resourceVolumeCreate,
        Read:   resourceVolumeRead,
        Delete: resourceVolumeDelete,

        Timeouts: &schema.ResourceTimeout{
	    Create: schema.DefaultTimeout(10 * time.Minute),
	    Delete: schema.DefaultTimeout(10 * time.Minute),
	},

        Schema: map[string]*schema.Schema{
            "create_time": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },

            "name": {
                Type:		schema.TypeString,
                Required:	true,
                ForceNew:       true,
            },

            "platform": {
                Type:	schema.TypeString,
                Required:	true,
                ForceNew:       true,
            },

            "project": {
                Type:           schema.TypeSet,
                Required:       true,
                ForceNew:       true,
                Elem: &schema.Resource{
                    Schema: map[string]*schema.Schema{
                        "id": {
                            Type:       schema.TypeInt,
                            Required:   true,
                        },

                        "name": {
                            Type:       schema.TypeString,
                            Optional:   true,
                            Computed:   true,
                        },
                    },
                },
            },

            "size": {
                Type: 		schema.TypeInt,
                Optional:	true,
                ForceNew:       true,
            },

            "status": {
                Type:           schema.TypeString,
                Optional:       true,
                Computed:       true,
            },

            "volume_id": {
                Type:           schema.TypeInt,
                Optional:       true,
                Computed:       true,
                ForceNew:       true,
            },

            "volume_uuid": {
                Type:		schema.TypeString,
                Optional:	true,
                Computed:	true,
            },
        },
    }
}

func resourceVolumeCreate(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    platform := d.Get("platform").(string)
    project := d.Get("project").(*schema.Set).List()[0]
    projectId := project.(map[string]interface{})["id"].(int)
    resourcePath := fmt.Sprintf("%s/volumes/", platform)
    name := d.Get("name").(string)
    body := VolumePostBody {
        Name:			name,
        Project:		projectId,
        Size:			d.Get("size").(int),
    }

    buf := new(bytes.Buffer)
    json.NewEncoder(buf).Encode(body)
    response, err := config.doNormalRequest(resourcePath, "POST", buf)

    if err != nil {
        return fmt.Errorf("Error creating gocloud_solution %s: %s", name, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }

    volumeID := int(data["id"].(float64))
    stateConf := &resource.StateChangeConf{
        Pending:    []string{"DOWNLOADING"},
        Target:     []string{"AVAILABLE", "CREATING", "ERROR"},
	Refresh:    volumeStateRefreshFunc(config, resourcePath, volumeID),
	Timeout:    d.Timeout(schema.TimeoutCreate),
	Delay:      10 * time.Second,
	MinTimeout: 3 * time.Second,
    }

    _, err = stateConf.WaitForState()
    if err != nil {
        return fmt.Errorf(
	    "Error waiting for gocloud_volume %d to become ready: %s", volumeID, err)
    }

    d.SetId(fmt.Sprintf("%v", data["id"]))
    d.Set("volume_id", data["id"])
    d.Set("platform", platform)
    d.Set("name", name)
    return resourceVolumeRead(d, meta)
}

func resourceVolumeRead(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    volumePlatform := d.Get("platform").(string)
    volumeID := d.Id()
    resourcePath := fmt.Sprintf("%s/volumes/%s/", volumePlatform, volumeID)
    response, err := config.doNormalRequest(resourcePath, "GET", nil)

    if err != nil {
        return fmt.Errorf("Unable to retrieve volume %s on %s: %s", volumeID, volumePlatform, err)
    }

    var data map[string]interface{}
    err = json.Unmarshal([]byte(response), &data)

    if err != nil {
        return err
    }

    log.Printf("[DEBUG] Retrieved gocloud_volume %s", d.Id())

    d.Set("create_time", data["create_time"])
    d.Set("size", data["size"])
    d.Set("status", data["status"])
    d.Set("volume_uuid", data["volume_uuid"])
    projectInfo := flattenVolumeProjectInfo(data["project"].(map[string]interface{}))
    d.Set("project", projectInfo)
    return nil
}

func resourceVolumeDelete(d *schema.ResourceData, meta interface{}) error {
    config := meta.(*PConfig)
    volumeID := d.Id()
    volumePlatform := d.Get("platform").(string)
    resourcePath := fmt.Sprintf("%s/volumes/%s/", volumePlatform, volumeID)
    _, err := config.doNormalRequest(resourcePath, "DELETE", nil)

    if err != nil {
        return fmt.Errorf("Unable to delete volume %s on %s: %s", volumeID, volumePlatform, err)
    }

    d.SetId("")

    return nil
}
