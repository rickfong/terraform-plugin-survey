package gocloud

import (
    "bytes"
    "crypto/tls"
    "io/ioutil"
    "net/http"
    "strings"
)

type ProviderClient struct {
    UserName 	string
    Password 	string
    Url 	string
    HTTPClient 	http.Client
}

func newProviderClient(username string, password string, url string) *ProviderClient {
    pc := ProviderClient{}
    pc.UserName = username
    pc.Password = password
    pc.Url = url
    is_https := strings.Contains(url, "https")

    if is_https {
        tr := &http.Transport{
            TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
        }
        client := http.Client{Transport: tr}
        pc.HTTPClient = client
    } else{
        client := http.Client{}
        pc.HTTPClient = client
    }

    return &pc
}

func (pc *ProviderClient) doRequest(
        resourcePath string,
        method string,
        body *bytes.Buffer) (string, error) {
    url :=  pc.Url + resourcePath
    var req *http.Request
    var err error

    if body != nil {
        req, err = http.NewRequest(method, url, body)
        req.Header.Set("Content-Type", "application/json")
    } else {
        req, err = http.NewRequest(method, url, nil)
    }

    if err != nil {
        return "Initial HTTP request failed", err
    }

    req.SetBasicAuth(pc.UserName, pc.Password)

    resp, err := pc.HTTPClient.Do(req)
    if err != nil {
        return "Sending request failed", err
    }

    okc := defaultOkCodes(method)
    var ok bool
    for _, code := range okc {
        if resp.StatusCode == code {
            ok = true
            break
        }
    }

    bodyBytes, err := ioutil.ReadAll(resp.Body)
    resp.Body.Close()

    if err != nil {
        return "Read response body failed", err
    }

    if !ok {
        respErr := ErrUnexpectedResponseCode{
            URL:            url,
            Method:         method,
            Expected:       okc,
            Actual:         resp.StatusCode,
            Body:           bodyBytes,
            ResponseHeader: resp.Header,
        }
        switch resp.StatusCode {
        case http.StatusBadRequest:
            err = ErrDefault400{respErr}
            if error400er, ok := err.(Err400er); ok {
                err = error400er.Error400(respErr)
            }
        case http.StatusNotFound:
            err = ErrDefault404{respErr}
            if error404er, ok := err.(Err404er); ok {
                err = error404er.Error404(respErr)
            }
        }
        if err == nil {
            err = respErr
        }
    }

    return string(bodyBytes), err
}

func defaultOkCodes(method string) []int {
    switch method {
    case "GET", "HEAD":
        return []int{200}
    case "POST":
        return []int{201, 202}
    case "PUT":
        return []int{201, 202}
    case "PATCH":
        return []int{200, 202, 204}
    case "DELETE":
        return []int{202, 204}
    }

    return []int{}
}
